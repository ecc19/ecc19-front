#!/bin/bash

# exit if a command returns a non-zero exit code and also print the commands and their args as they are executed
set -e -x

# Add the pulumi CLI to the PATH
export PATH=$PATH:$HOME/.pulumi/bin

rm ./pulumi/package-lock.json
cd ./pulumi
yarn install
pulumi stack select prod
# The following is just a sample config setting that the hypothetical pulumi
# program needs.
# Learn more about pulumi configuration at: https://www.pulumi.com/reference/config/
# pulumi config set static-website:pathToWebsiteContents ../dist/ecc19-front
pulumi up --yes > ./pulumi-log.txt
