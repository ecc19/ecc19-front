import {ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {ApiService} from '../../../api/api.service';
import {faCrosshairs, faSearch} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-layout',
  templateUrl: './thanks.component.html',
  styleUrls: ['./thanks.component.scss']
})
export class ThanksComponent implements OnInit {
  @ViewChild('address') addressInput: any;

  confirmOk = false;
  pharmacies;
  faCrosshair = faCrosshairs;
  faSearch = faSearch;
  location: string;
  latLng: string;
  isSearching: boolean;
  isLocalizing: boolean;

  constructor(
    private apiService: ApiService,
    private ref: ChangeDetectorRef
  ) {
  }

  ngAfterViewInit() {
  }

  ngOnInit(): void {

  }

  confirmation() {
    this.confirmOk = true;
    this.initPlaceAutocomplete();
  }

  initPlaceAutocomplete() {
    const autocomplete = new google.maps.places.Autocomplete(this.addressInput.nativeElement,
      {
        componentRestrictions: { country: 'FR' },
        types: ['geocode']  // 'establishment' / 'address' / 'geocode'
      });
    google.maps.event.addListener(autocomplete, 'place_changed', () => {
      const place = autocomplete.getPlace();
      this.location = place.formatted_address;
      this.latLng = place.geometry.location.toUrlValue();
      this.ref.detectChanges();
    });
  }

  findMe() {
    if (navigator.geolocation) {
      this.isLocalizing = true;
      navigator.geolocation.getCurrentPosition((position) => {
        this.location = position.coords.latitude + ',' + position.coords.longitude;
        this.latLng = this.location;
        this.isLocalizing = false;
      }, positionError => {
        this.isLocalizing = false;
        alert('Impossible de vous localiser: ' + positionError.message)
      });
    }
  }

  searchPharmacies() {
    this.isSearching = true;
    this.apiService.getPharmacies(this.latLng)
      .subscribe(res => {
        console.log(res);
        this.pharmacies = res.results.map(place => ({
          name: place.name,
          address: place.vicinity,
          link: `https://www.google.com/maps/search/?api=1&query=${place.geometry.location.toString()}&query_place_id=${place.place_id}`
        }));

        this.isSearching = false;
      }, err => {
        console.error(err);
        this.isSearching = false;
      });
  }

  localize() {
    console.log('localize');
  }
}
