import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ThanksRoutingModule} from './thanks-routing.module';
import {ThanksComponent} from './components/thanks/thanks.component';
import {CoreModule} from '../core/core.module';
import {ApiModule} from '../api/api.module';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {FormsModule} from '@angular/forms';


@NgModule({
  declarations: [ThanksComponent],
  imports: [
    CommonModule,
    ThanksRoutingModule,
    CoreModule,
    FontAwesomeModule,
    ApiModule,
    FormsModule
  ]
})
export class ThanksModule { }
