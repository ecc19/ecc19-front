import * as _ from 'lodash';

export function indexListBy<T>(list: T[], propOrFn: string | ((elem: T) => string)): { [key: string]: T[] } {
  const keyFn = (typeof propOrFn === 'string') ? (el) => _.get(el, propOrFn) : (el) => propOrFn(el);

  return _.reduce(list, (acc, el) => {
    const key = keyFn(el);
    acc[key] = acc[key] || [];
    acc[key].push(el);
    return acc;
  }, {});
}
