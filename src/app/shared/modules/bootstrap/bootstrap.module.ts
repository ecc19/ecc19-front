import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TabsModule} from 'ngx-bootstrap/tabs';
import {BsDropdownModule} from 'ngx-bootstrap/dropdown';
import {ButtonsModule} from 'ngx-bootstrap/buttons';
import {AccordionModule} from 'ngx-bootstrap/accordion';
import {BsDatepickerModule, BsLocaleService} from 'ngx-bootstrap/datepicker';
import {CollapseModule} from 'ngx-bootstrap/collapse';
import {defineLocale, frLocale} from 'ngx-bootstrap/chronos';
import {ModalModule} from 'ngx-bootstrap/modal';

defineLocale('fr', frLocale);

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    TabsModule.forRoot(),
    BsDropdownModule.forRoot(),
    AccordionModule.forRoot(),
    ButtonsModule.forRoot(),
    BsDatepickerModule.forRoot(),
    CollapseModule.forRoot(),
    ModalModule.forRoot()
  ],
  exports: [
    TabsModule,
    BsDropdownModule,
    AccordionModule,
    ButtonsModule,
    BsDatepickerModule,
    CollapseModule,
    ModalModule
  ]
})
export class BootstrapModule {

  constructor(private bsLocaleService: BsLocaleService) {
    this.bsLocaleService.use('fr');
  }
}
