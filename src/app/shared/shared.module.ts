import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BootstrapModule } from './modules/bootstrap/bootstrap.module';
import { NgMultiSelectModule } from './modules/ng-multi-select/ng-multi-select.module';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    BootstrapModule,
    NgMultiSelectModule,
    FormsModule
  ],
  exports: [
    BootstrapModule,
    NgMultiSelectModule,
    FormsModule
  ]
})
export class SharedModule {
}
