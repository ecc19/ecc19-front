import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TunnelButtonsComponent} from './tunnel-buttons.component';

describe('TunnelButtonsComponent', () => {
  let component: TunnelButtonsComponent;
  let fixture: ComponentFixture<TunnelButtonsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TunnelButtonsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TunnelButtonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
