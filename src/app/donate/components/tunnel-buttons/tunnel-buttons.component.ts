import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-tunnel-buttons',
  templateUrl: './tunnel-buttons.component.html',
  styleUrls: ['./tunnel-buttons.component.scss']
})
export class TunnelButtonsComponent implements OnInit {
  @Input() currentIndex: number;

  @Output() submit = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

}
