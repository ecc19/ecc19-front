import {Component} from '@angular/core';
import {deserialize, serialize} from 'class-transformer';
import {Offering} from '../../../api/model/offering';
import {Router} from '@angular/router';

@Component({
  selector: 'app-step-user-info',
  templateUrl: './step-user-info.component.html',
  styleUrls: ['./step-user-info.component.scss']
})
export class StepUserInfoComponent {
  offering: Offering;
  error: string;

  constructor(
    private router: Router
  ) {
    this.offering = localStorage.currentOffering ?
      deserialize(Offering, localStorage.currentOffering) : new Offering();
  }

  ngOnInit(): void {

  }

  public submit() {
    if (!this.offering.name || !this.offering.email || !this.offering.phone) {
      this.error = 'Merci de renseigner les champs obligatoires *'
      return;
    }

    // save to local storage
    localStorage.currentOffering = serialize(this.offering);
    console.log(this.offering);

    // navigate to next step
    this.router.navigateByUrl('/donate/5');
  }
}
