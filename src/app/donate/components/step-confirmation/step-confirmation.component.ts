import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {equipmentIcons} from '../../constants/material-icon-type.constant';
import {deserialize} from 'class-transformer';
import {Offering} from '../../../api/model/offering';
import {Router} from '@angular/router';
import {ApiService} from '../../../api/api.service';
import {GoogleAnalyticsService} from '../../../core/services/google-analytics.service';

@Component({
  selector: 'app-step-confirmation',
  templateUrl: './step-confirmation.component.html',
  styleUrls: ['./step-confirmation.component.scss']
})
export class StepConfirmationComponent implements OnInit {

  @Output() confirmationEventEmitter = new EventEmitter();

  icons = equipmentIcons;
  offering: Offering;
  error: string;

  constructor(
    private router: Router,
    private apiService: ApiService,
    private googleAnalyticsService: GoogleAnalyticsService
  ) {
    this.offering = localStorage.currentOffering ?
      deserialize(Offering, localStorage.currentOffering) : new Offering();
  }

  ngOnInit(): void {
  }

  submit() {
    delete this.error;
    this.apiService.addOffering(this.offering).subscribe(offering => {
      delete localStorage.currentOffering;
      this.googleAnalyticsService.emitEvent(
        'new_donation',
        'donation',
        'Nouvelle donation',
        1
      );
      return this.router.navigate(['/thanks']);
    }, error => {
      this.error = error;
    });
  }
}
