import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-tunnel-steps',
  templateUrl: './tunnel-steps.component.html',
  styleUrls: ['./tunnel-steps.component.scss']
})
export class TunnelStepsComponent implements OnInit {
  @Input() currentIndex: number;

  steps = ['Sélection', 'Matériel', 'Lieu', 'Vos informations', 'Confirmation'];
  constructor() { }

  ngOnInit(): void {
  }

}
