import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TunnelStepsComponent} from './tunnel-steps.component';

describe('StepsComponent', () => {
  let component: TunnelStepsComponent;
  let fixture: ComponentFixture<TunnelStepsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TunnelStepsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TunnelStepsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
