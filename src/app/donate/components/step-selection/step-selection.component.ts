import {Component} from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {IDropdownSettings} from 'ng-multiselect-dropdown/multiselect.model';
import {deserialize, serialize} from 'class-transformer';
import {Offering} from '../../../api/model/offering';
import * as _ from 'lodash';
import {OfferingItem} from '../../../api/model/offering-item';
import {Router} from '@angular/router';

@Component({
  selector: 'app-step-selection',
  templateUrl: './step-selection.component.html',
  styleUrls: ['./step-selection.component.scss']
})
export class StepSelectionComponent {

  options = [
    {id: 'surblouses', label: 'Surblouses de protection manches longues'},
    {id: 'salopettes', label: 'Salopettes ou combinaisons intégrales'},
    {id: 'bonnets', label: 'Bonnets ou charlottes'},
    {id: 'frottis', label: 'Frottis nasopharyngés'},
    {id: 'desinfectantsol', label: 'Désinfectant de sol'},
    {id: 'desinfectantsurface', label: 'Désinfectant de surface'}
  ];
  optionSelected = [];
  dropdownSettings: IDropdownSettings = {
    singleSelection: false,
    enableCheckAll: false,
    allowSearchFilter: false,
    idField: "id",
    textField: "label",
  };

  offering: Offering;
  checkboxValue: any = {};

  error: string;

  constructor(
    private fb: FormBuilder,
    private router: Router
  ) {
    this.offering = localStorage.currentOffering ?
      deserialize(Offering, localStorage.currentOffering) : new Offering();

    this.offering.items.forEach(item => {
      const option = this.options.find(o => o.id === item.type);

      if (option) {
        this.optionSelected.push(option); // Autres...
      } else {
        this.checkboxValue[item.type] = true  // checkboxs
      }
    });
  }

  submit() {
    const selectedTypes = _.keys(this.checkboxValue)
      .filter(type => this.checkboxValue[type] == true)
      .concat(this.optionSelected.map(o => o.id));

    if (selectedTypes.length == 0) {
      this.error = 'Veuillez sélectionner au moins un type de matériel';
      return;
    }

    // remove items not selected
    _.remove(this.offering.items, i => !_.includes(selectedTypes, i.type));

    // add missing items
    _.difference(selectedTypes, this.offering.items.map(i => i.type)).forEach(type => {
      this.offering.items.push(new OfferingItem({type: type}))
    });

    // save to local storage
    localStorage.currentOffering = serialize(this.offering);
    console.log(this.offering);

    // navigate to next step
    this.router.navigateByUrl('/donate/2');
  }
}
