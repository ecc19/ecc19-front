import {Component} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {deserialize, serialize} from 'class-transformer';
import {Offering} from '../../../api/model/offering';
import {equipmentIcons} from '../../constants/material-icon-type.constant';
import {Router} from '@angular/router';

@Component({
  selector: 'app-step-equipment',
  templateUrl: './step-equipment.component.html',
  styleUrls: ['./step-equipment.component.scss']
})
export class StepEquipmentComponent {

  offering: Offering;
  form: FormGroup;
  icons = equipmentIcons;
  error: string;

  constructor(
    private router: Router
  ) {
    this.offering = localStorage.currentOffering ?
      deserialize(Offering, localStorage.currentOffering) : new Offering();
  }

  ngOnInit(): void {

  }

  submit() {
    // check validity
    for (let item of this.offering.items) {
      if (!item.quantity) {
        this.error = 'Merci de remplir toutes les quantités';
        return;
      }
    }

    delete this.error;

    // save to local storage
    localStorage.currentOffering = serialize(this.offering);
    console.log(this.offering);

    // navigate to next step
    this.router.navigateByUrl('/donate/3');
  }
}
