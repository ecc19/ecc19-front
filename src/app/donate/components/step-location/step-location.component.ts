import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {deserialize, serialize} from 'class-transformer';
import {Offering} from '../../../api/model/offering';

@Component({
  selector: 'app-step-location',
  templateUrl: './step-location.component.html',
  styleUrls: ['./step-location.component.scss']
})
export class StepLocationComponent {

  isSameLocation = false;
  offering: Offering;
  error: string;

  constructor(
    private router: Router
  ) {
    this.offering = localStorage.currentOffering ?
      deserialize(Offering, localStorage.currentOffering) : new Offering();
  }

  changeCb() {
    this.isSameLocation = !this.isSameLocation;
  }

  submit() {
    if (!this.isValid()) {
      this.error = 'Veuillez renseigner la localisation du matériel';
      return;
    }

    // copy value
    if (this.isSameLocation) {
      const postalCode = this.offering.items[0].postalCode;
      const city = this.offering.items[0].city;

      this.offering.items.forEach(i => {
        i.postalCode = postalCode;
        i.city = city;
      });
    }

    // save to local storage
    localStorage.currentOffering = serialize(this.offering);
    console.log(this.offering);

    // navigate to next step
    this.router.navigateByUrl('/donate/4');
  }

  private isValid() {
    if (this.isSameLocation) {
      const postalCode = this.offering.items[0].postalCode;
      const city = this.offering.items[0].city;
      if (!postalCode || !city) {
        return false;
      }
    } else {
      for (let item of this.offering.items) {
        if (!item.postalCode || !item.city) {
          return false;
        }
      }
    }

    return true;
  }
}
