import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {StepSelectionComponent} from './components/step-selection/step-selection.component';
import {StepEquipmentComponent} from './components/step-equipment/step-equipment.component';
import {StepLocationComponent} from './components/step-location/step-location.component';
import {StepUserInfoComponent} from './components/step-user-info/step-user-info.component';
import {StepConfirmationComponent} from './components/step-confirmation/step-confirmation.component';


const routes: Routes = [
  { path: '', redirectTo: '1', pathMatch: 'full' },
  {
    path: '1',
    component : StepSelectionComponent
  },
  {
    path: '2',
    component : StepEquipmentComponent
  },
  {
    path: '3',
    component : StepLocationComponent
  },
  {
    path: '4',
    component : StepUserInfoComponent
  },
  {
    path: '5',
    component : StepConfirmationComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DonateRoutingModule { }
