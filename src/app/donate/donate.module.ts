import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {DonateRoutingModule} from './donate-routing.module';
import {StepSelectionComponent} from './components/step-selection/step-selection.component';
import {StepEquipmentComponent} from './components/step-equipment/step-equipment.component';
import {SharedModule} from '../shared/shared.module';
import {ReactiveFormsModule} from '@angular/forms';
import {StepLocationComponent} from './components/step-location/step-location.component';
import {StepUserInfoComponent} from './components/step-user-info/step-user-info.component';
import {StepConfirmationComponent} from './components/step-confirmation/step-confirmation.component';
import {ApiModule} from '../api/api.module';
import {TunnelStepsComponent} from './components/tunnel-steps/tunnel-steps.component';
import {TunnelButtonsComponent} from './components/tunnel-buttons/tunnel-buttons.component';


@NgModule({
  declarations: [
    StepSelectionComponent,
    StepEquipmentComponent,
    StepLocationComponent,
    StepUserInfoComponent,
    StepConfirmationComponent,
    TunnelStepsComponent,
    TunnelButtonsComponent
  ],
  imports: [
    CommonModule,
    DonateRoutingModule,
    SharedModule,
    ReactiveFormsModule,
    ApiModule
  ]
})
export class DonateModule {
}
