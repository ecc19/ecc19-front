export enum RichTextItemType {
  heading1 = 'heading1',
  paragraph = 'paragraph',
}

export class RichTextItem {
  type: RichTextItemType;
  text: string;
  spans: [];
}
