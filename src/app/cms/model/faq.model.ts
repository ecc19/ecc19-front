export interface FaqQuestion {
  title: string;
  text: string;
}
export interface FaqCategory {
  label: string;
  questions: FaqQuestion[];
}
