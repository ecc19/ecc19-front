import {Injectable} from '@angular/core';
import * as Prismic from 'prismic-javascript';
import * as _ from 'lodash';
import {indexListBy} from '../../utils/functions';
import {FaqCategory} from '../model/faq.model';
import * as PrismicDOM from 'prismic-dom';
import {Document} from 'prismic-javascript/d.ts/documents';
import {BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PrismicService {
  apiEndpoint = "https://plateform-ecc19.cdn.prismic.io/api/v2";
  apiToken = "MC5YbzNhVVJBQUFMbFR3bVlI.B--_ve-_ve-_ve-_vUDvv71MBXpt77-9JO-_ve-_vQVbUSTvv70f77-977-977-9BVoW77-977-977-977-9RQ";
  docsByType: { [p: string]: Document[] };
  faqCategories$ = new BehaviorSubject<FaqCategory[]>(null);

  cguText: string;
  cookiesText: string;
  confidText: string;
  aproposText: string;
  contactText: string;

  constructor() {}

  private async initApi() {
    return Prismic.getApi(this.apiEndpoint, {accessToken: this.apiToken});
  }

  async preloadData() {
    const api = await this.initApi();
    this.docsByType = await api.query('', { pageSize : 100 })
      .then(res => indexListBy(res.results, 'type'));

    this.processFaqs();
    this.processPages();
  }

  private processFaqs() {
    this.docsByType.faqcategory.sort((a, b) => a.data.order - b.data.order)
    const catsByName = _.keyBy(this.docsByType.faqcategory, 'slugs[0]');
    const faqByCat = indexListBy(this.docsByType.faq, 'data.category.slug');

    const faqCategories = _.keys(catsByName).map(catId => ({
      label: catsByName[catId].data.name,
      questions: (faqByCat[catId] || []).sort((a, b) => a.data.order - b.data.order).map(faq => ({
        title: PrismicDOM.RichText.asText(faq.data.title, linkResolver),
        text: PrismicDOM.RichText.asHtml(faq.data.text, linkResolver)
      }))
    }));
    this.faqCategories$.next(faqCategories);
  }

  private processPages() {
    this.aproposText = PrismicDOM.RichText.asHtml(this.docsByType['page-apropos'][0].data.content, linkResolver);
    this.cguText = PrismicDOM.RichText.asHtml(this.docsByType['page-cgu'][0].data.content, linkResolver);
    this.cookiesText = PrismicDOM.RichText.asHtml(this.docsByType['page-cookies'][0].data.content, linkResolver);
    this.confidText = PrismicDOM.RichText.asHtml(this.docsByType['page-confiden'][0].data.content, linkResolver);
    this.contactText = PrismicDOM.RichText.asHtml(this.docsByType['page-contact'][0].data.content, linkResolver);
  }
}

// FIXME à adapter
const linkResolver = function(doc) {
  // Pretty URLs for known types
  if (doc.type === 'page-confiden') return "/home/confidentialite";
  if (doc.type === 'page-cgu') return "/home/cgu";
  if (doc.type === 'page-cookies') return "/home/cookies";
  if (doc.type === 'page-contact') return "/home/contact";
  if (doc.type === 'page-apropos') return "/home/apropos";
  // Fallback for other types, in case new custom types get created
  return "/";
};
