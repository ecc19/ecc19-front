import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PageNotFoundComponent} from './components/page-not-found/page-not-found.component';
import {HeaderComponent} from './components/header/header.component';
import {FooterComponent} from './components/footer/footer.component';
import {RouterModule} from '@angular/router';
import {BootstrapModule} from '../shared/modules/bootstrap/bootstrap.module';
import {HttpClientModule} from '@angular/common/http';


@NgModule({
  declarations: [PageNotFoundComponent, HeaderComponent, FooterComponent],
  imports: [
    RouterModule,
    CommonModule,
    BootstrapModule,
    HttpClientModule,
  ],
  exports: [
    HeaderComponent, FooterComponent
  ]
})
export class CoreModule { }
