import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Offering} from './model/offering';
import {environment} from '../../environments/environment';
import {tap} from 'rxjs/operators';
import {Observable, of} from 'rxjs';
import {PlaceSearchResponse} from './dto/google-maps.dto';
import {Message} from './model/message';

@Injectable()
export class ApiService {
  private messages: string[] = [];

  headers = new HttpHeaders({ 'Content-Type': 'application/json' });
  httpOptions = {
    headers: this.headers
  };

  constructor(
    private httpClient: HttpClient
  ) {

  }

  public addOffering(offering: Offering): Observable<Offering> {
    return this.httpClient.post<Offering>(`${environment.baseUrl}/offerings`, offering, this.httpOptions).pipe(
      tap((offering: Offering) => this.log(`added offering w/ id=${offering.id}`))
      // catchError(this.handleError<Offering>('addOffering'))
    );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** Log the error */
  private log(message: string) {
    // TODO do something with the messages
    this.messages.push(`ApiService: ${message}`);
  }

  getPharmacies(query: string) {
    return this.httpClient.get<PlaceSearchResponse>(`${environment.baseUrl}/places/pharmacies`, {
      headers: this.headers,
      params: { query }
    });
  }

  public addMessage(message: Message): Observable<Message> {
    return this.httpClient.post<Message>(`${environment.baseUrl}/messages`, message, this.httpOptions);
  }

}
