import {FileData} from './file-data';
import {EquipmentType} from './equipment-type.enum';

export class OfferingItem {
  constructor(params?: any) {
    if (params) {
      Object.assign(this, params);
    }
  }

  id: number;
  quantity: number;
  type: EquipmentType;
  expirationDate: Date;
  description: string;
  standard: string;
  photos: FileData[];
  postalCode: string;

  city: string;
}
