import {IsEmail, IsNotEmpty} from 'class-validator';

export class Message {

  @IsNotEmpty()
  subject: string;

  @IsNotEmpty()
  name: string;

  @IsEmail()
  email: string;

  @IsNotEmpty()
  content: string;
}
