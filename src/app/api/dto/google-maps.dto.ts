import PlacesServiceStatus = google.maps.places.PlacesServiceStatus;
import PlaceResult = google.maps.places.PlaceResult;

export class PlaceSearchResponse {
  html_attributions: []
  next_page_token: string;
  results: PlaceResult[];
  status: PlacesServiceStatus;
}
