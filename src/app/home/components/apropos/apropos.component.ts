import {Component, OnInit} from '@angular/core';
import {PrismicService} from '../../../cms/services/prismic.service';

@Component({
  selector: 'app-apropos',
  templateUrl: './apropos.component.html',
  styleUrls: ['./apropos.component.scss']
})
export class AproposComponent implements OnInit {

  constructor(
    public prismic: PrismicService
  ) { }

  ngOnInit(): void {
  }

}
