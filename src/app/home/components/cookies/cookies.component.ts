import {Component, OnInit} from '@angular/core';
import {PrismicService} from '../../../cms/services/prismic.service';

@Component({
  selector: 'app-cookies',
  templateUrl: './cookies.component.html',
  styleUrls: ['./cookies.component.scss']
})
export class CookiesComponent implements OnInit {

  constructor(
    public prismic: PrismicService
  ) { }

  ngOnInit(): void {
  }

}
