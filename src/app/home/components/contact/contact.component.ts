import {Component, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {PrismicService} from '../../../cms/services/prismic.service';
import {ApiService} from '../../../api/api.service';
import {Message} from '../../../api/model/message';
import {validate} from 'class-validator';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {
  @ViewChild('modal') modal;

  message = new Message();
  error: string;
  isSending: boolean;
  modalRef: BsModalRef;

  constructor(
    public prismic: PrismicService,
    private apiService: ApiService,
    private router: Router,
    private modalService: BsModalService
  ) {
  }

  ngOnInit(): void {

  }

  async sendMessage() {
    const errors = await validate(this.message)
    if (errors.length > 0) {
      this.error = 'Merci de renseigner tous les champs';
      console.log(errors);
      return;
    }

    this.isSending = true;
    this.apiService.addMessage(this.message).subscribe(message => {
      this.isSending = false;
      this.modalRef = this.modalService.show(this.modal);
    }, error => {
      this.isSending = false;
      this.error = 'Envoi du message échoué. Veuillez réessayer ultérieurement';
    })
  }

  closeModal() {
    this.router.navigate(['/']);
    this.modalRef.hide();
  }

}
