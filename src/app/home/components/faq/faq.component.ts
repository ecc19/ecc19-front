import {Component, OnInit} from '@angular/core';
import {faChevronDown, faChevronUp} from '@fortawesome/free-solid-svg-icons';
import {PrismicService} from '../../../cms/services/prismic.service';
import {FaqCategory} from '../../../cms/model/faq.model';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.scss']
})
export class FaqComponent implements OnInit {
  faChevronDown = faChevronDown;
  faChevronUp = faChevronUp;

  faqCategories: any[];
  currentCat: FaqCategory;
  private subscription: Subscription;

  constructor(
    private prismicService: PrismicService
  ) {

  }

  ngOnInit(): void {
    this.subscription = this.prismicService.faqCategories$.subscribe(cats => {
      this.faqCategories = cats;
      if (!!cats && cats.length > 0) { this.setCurrent(cats[0])}
    })
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  setCurrent(cat: FaqCategory) {
    this.currentCat = cat;
  }
}
