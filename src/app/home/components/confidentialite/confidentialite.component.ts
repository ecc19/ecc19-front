import {Component, OnInit} from '@angular/core';
import {PrismicService} from '../../../cms/services/prismic.service';

@Component({
  selector: 'app-confidentialite',
  templateUrl: './confidentialite.component.html',
  styleUrls: ['./confidentialite.component.scss']
})
export class ConfidentialiteComponent implements OnInit {

  constructor(
    public prismic: PrismicService
  ) { }

  ngOnInit(): void {
  }

}
