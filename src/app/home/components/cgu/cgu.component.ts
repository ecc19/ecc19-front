import {Component, OnInit} from '@angular/core';
import {PrismicService} from '../../../cms/services/prismic.service';

@Component({
  selector: 'app-cgu',
  templateUrl: './cgu.component.html',
  styleUrls: ['./cgu.component.scss']
})
export class CguComponent implements OnInit {
  constructor(
    public prismic: PrismicService
  ) { }

  ngOnInit(): void {
  }
}
