import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {AccueilRoutingModule} from './home-routing.module';
import {LayoutComponent} from './components/layout/layout.component';
import {AproposComponent} from './components/apropos/apropos.component';
import {FaqComponent} from './components/faq/faq.component';
import {ContactComponent} from './components/contact/contact.component';
import {BootstrapModule} from '../shared/modules/bootstrap/bootstrap.module';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {CmsModule} from '../cms/cms.module';
import {CguComponent} from './components/cgu/cgu.component';
import {CookiesComponent} from './components/cookies/cookies.component';
import {ConfidentialiteComponent} from './components/confidentialite/confidentialite.component';
import {ApiModule} from '../api/api.module';
import {FormsModule} from '@angular/forms';


@NgModule({
  declarations: [LayoutComponent, AproposComponent, FaqComponent, ContactComponent, CguComponent, CookiesComponent, ConfidentialiteComponent],
  imports: [
    CommonModule,
    AccueilRoutingModule,
    BootstrapModule,
    FontAwesomeModule,
    CmsModule,
    FormsModule,
    ApiModule
  ]
})
export class HomeModule { }
