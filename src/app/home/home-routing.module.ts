import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LayoutComponent} from './components/layout/layout.component';
import {AproposComponent} from './components/apropos/apropos.component';
import {FaqComponent} from './components/faq/faq.component';
import {ContactComponent} from './components/contact/contact.component';
import {CguComponent} from './components/cgu/cgu.component';
import {CookiesComponent} from './components/cookies/cookies.component';
import {ConfidentialiteComponent} from './components/confidentialite/confidentialite.component';


const routes: Routes = [
  {
    path :'',
    component: LayoutComponent
  },
  {
    path :'apropos',
    component: AproposComponent
  },
  {
    path :'faq',
    component: FaqComponent
  },
  {
    path :'contact',
    component: ContactComponent
  },
  {
    path :'cgu',
    component: CguComponent
  },
  {
    path :'cookies',
    component: CookiesComponent
  },
  {
    path :'confidentialite',
    component: ConfidentialiteComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccueilRoutingModule { }
