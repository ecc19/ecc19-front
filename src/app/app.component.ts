import {Component} from '@angular/core';
import {PrismicService} from './cms/services/prismic.service';
import {NavigationEnd, Router} from '@angular/router';

declare let gtag: Function;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  title = 'ecc19-front';

  constructor(
    private prismic: PrismicService,
    private router: Router
  ) {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        gtag('config', 'UA-163435930-1', {
            'page_path': event.urlAfterRedirects
        });
      }
    });
  }

  ngOnInit() {
    this.prismic.preloadData();
  }

  ngAfterViewInit(): void {
    // Load google maps script after view init
    const DSLScript = document.createElement('script');
    DSLScript.src = 'https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyDIOTUMNlIC3ExrHnqlMB9cigLFdKrWxbY'; // replace by your API key
    DSLScript.type = 'text/javascript';
    document.body.appendChild(DSLScript);
    document.body.removeChild(DSLScript);
  }
}
